import { LitElement, html } from 'lit-element';
import styles from './app-header-style.js';


class AppHeader extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor() {
        super()

    }

    static get shadyStyles() {
        return `
            ${styles.cssText}
          `;
    }

    render() {
        return html`
        <style>
            ${this.constructor.shadyStyles}
        </style>
        <header>
            <img src="../../assets/rick.png">
            <img src="../../assets/descarga.jpg">
        </header>
        `;
    }

}

customElements.define("app-header", AppHeader);
//("etiqueta", clase a la que se refiere)