import { LitElement, html } from 'lit-element';
import styles from './app-main-styles.js';
import '../app-dm/app-dm.js';
import '../app-file-list/app-file-list.js';


class AppMain extends LitElement {

    static get properties() {
        return {
            characters: {type: Object}
        };
    }

    constructor() {
        super()

        this.characters = {
            info: {},
            results: []
        };
    }

    static get shadyStyles() {
        return `
            ${styles.cssText}
          `;
    }

    render() {
        return html`
        <style>
            ${this.constructor.shadyStyles}
        </style>
        <section>
            <app-dm @set-characters="${this.setCharacters}"></app-dm>
            <div class="fichaMain" id="idCharacters">
                ${this.characters.results.map(
                    character => html`
                    <app-file-list 
                        class="fichaAppFileList"
                        name="${character.name}"
                        status="${character.status}"
                        species="${character.species}"
                        gender="${character.gender}"
                        img="${character.image}"
                        locationUrl="${character.location.url}"
                        locationName="${character.location.name}"
                        .disabledLocation="${character.location.disabledLocation}"
                        @get-location="${this.getLocation}"
                    ></app-file-list>`
                )}
            </div>
        </section>
        `;
    }

    setCharacters(e) {
        this.characters = e.detail.characters;

        this.dispatchEvent(
            new CustomEvent("set-pages", {
                detail: {
                    characters: this.characters
                }
            })
        );
    }

    getLocation(e) {
        this.shadowRoot.querySelector("app-dm").urlLocation = "";
        this.shadowRoot.querySelector("app-dm").urlLocation = e.detail.url;
    }

}

customElements.define("app-main", AppMain);