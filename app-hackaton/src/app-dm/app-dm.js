import { LitElement, html } from 'lit-element';


class AppDm extends LitElement{

    static get properties() {
        return {
            characters: {type: Object},
            url: {type: String},
            urlLocation: {type: String},
            residents: {type: Object}
        };
    }

    constructor() {
        super();

        this.characters = {};
        this.getAllCharacters();
    }

    updated(changedProperties) {

        if (changedProperties.has("characters")) {
            this.dispatchEvent(
                new CustomEvent("set-characters", {
                    bubbles: true,
                    composed: true,
                    detail: {
                        characters: this.characters
                    }
                })
            );
        }

        if (changedProperties.has("url")) {
            if (!(!this.url || /^\s*$/.test(this.url))) {
                this.getPage();
            }
        }

        if (changedProperties.has("urlLocation")) {
            if (!(!this.urlLocation || /^\s*$/.test(this.urlLocation))) {
                this.getLocation();
            }
        }

        if (changedProperties.has("residents")) {
            this.getAllResidents();
        }
    }

    async getAllResidents() {
        let residentLength = this.residents.length > 5 ? 5 : this.residents.length;
        let moreResi = this.residents.length > 5 ? true : false;
        let allRes = [];
        let numResi = 0;
        for (var i=0; i < residentLength; i++) {
            numResi = moreResi ? this.randomIntFromInterval(0, this.residents.length - 1) : i;
            let result = await this.getResident(this.residents[numResi]);
            result.location.disabledLocation = true;
            allRes = [...allRes, result];
        }
        this.characters = {};
        this.characters.results = allRes;
        this.characters.info = {
            location: true,
            moreResidents: moreResi
        };
    }

    getAllCharacters() {
        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                let APIResponse = JSON.parse(xhr.responseText);
                this.characters = APIResponse;
            }
        }

        xhr.open("GET", "https://rickandmortyapi.com/api/character/");
        xhr.send();
    }

    getPage() {

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                let APIResponse = JSON.parse(xhr.responseText);
                this.characters = APIResponse;
            }
        }

        xhr.open("GET", this.url);
        xhr.send();
    }

    getLocation() {

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                let APIResponse = JSON.parse(xhr.responseText);
                this.residents = APIResponse.residents;
            }
        }

        xhr.open("GET", this.urlLocation);
        xhr.send();
    }

    async getResident(urlResident) {

        const response = await fetch(urlResident);
        const responseJson = await response.json();

        return responseJson;
    }

    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

}

customElements.define('app-dm', AppDm);
