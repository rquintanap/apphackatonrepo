import { LitElement, html } from 'lit-element';
import '../app-header/app-header';
import '../app-main/app-main';
import '../app-footer/app-footer.js';
import '../app-sidebar/app-sidebar.js';

class AppHackaton extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <app-header></app-header>
            <app-sidebar @set-characters="${this.setCharacters}"></app-sidebar>
            <div>
                <app-main @set-pages="${this.setPages}"></app-main>
            </div>
           
            <app-footer></app-footer>
        `;
    }

    setPages(e) {

        if (e.detail.characters.info) {
            this.shadowRoot.querySelector("app-sidebar").pagePrevious = !e.detail.characters.info.prev || /^\s*$/.test(e.detail.characters.info.prev);
            if (!this.shadowRoot.querySelector("app-sidebar").pagePrevious) {
                this.shadowRoot.querySelector("app-sidebar").idPrevious = e.detail.characters.info.prev;
                this.shadowRoot.querySelector("app-sidebar").page = parseInt(e.detail.characters.info.prev.split("=")[1])+1;
            } else {
                this.shadowRoot.querySelector("app-sidebar").page = "1";
            }

            this.shadowRoot.querySelector("app-sidebar").pageNext = !e.detail.characters.info.next || /^\s*$/.test(e.detail.characters.info.next);
            if (!this.shadowRoot.querySelector("app-sidebar").pageNext) {
                this.shadowRoot.querySelector("app-sidebar").idNext = e.detail.characters.info.next;
            }

            this.shadowRoot.querySelector("app-sidebar").subTitle = "";
            if (!e.detail.characters.info.location || /^\s*$/.test(e.detail.characters.info.location)) {
                this.shadowRoot.querySelector("app-sidebar").maxPages = e.detail.characters.info.pages;
                this.shadowRoot.querySelector("app-sidebar").pageCharacters = true;
                this.shadowRoot.querySelector("app-sidebar").title = "Characters";
            } else {
                this.shadowRoot.querySelector("app-sidebar").maxPages = "1";
                this.shadowRoot.querySelector("app-sidebar").pageCharacters = false;
                this.shadowRoot.querySelector("app-sidebar").title = "Location: " + e.detail.characters.results[0].location.name;
                if (e.detail.characters.info.moreResidents) {
                    this.shadowRoot.querySelector("app-sidebar").subTitle = "Example of 5 random residents";
                } else {
                    this.shadowRoot.querySelector("app-sidebar").subTitle = "All residents";
                }
            }
        }
    }

    setCharacters(e) {
        this.shadowRoot.querySelector("app-main").characters = e.detail.characters;
        this.setPages(e);
    }

}

customElements.define('app-hackaton', AppHackaton);
