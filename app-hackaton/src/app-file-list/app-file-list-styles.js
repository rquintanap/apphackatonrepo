import { css } from 'lit-element';

export default css`
    .ficha{
        display:flex;
        flex-direction:row;
        width:700px;
        height:auto;
        background-color:#3c3e44;
        -webkit-box-shadow: 1px 1px 5px 1px #000000; 
        box-shadow: 1px 1px 5px 1px #000000;
        border-radius: 15px;
        margin: 10px;
        margin-top: 35px;
    }

    .img img{
        // border: 1px solid red;
        width: auto;
        // margin-top: 5px;
        height: 100%;
        border-radius: 13px 0px 0px 13px;
    }

    .data {
        width:100%;
        display:flex;
        flex-direction:column;
    }
    .titleDiv{
        color:White;
        padding-left:20px;
    }
    .dataDiv{
        color:White;
        padding-left:20px;
        padding-bottom:10px;
        padding-top:10px;
    }

    .colorTitle{
        color:#00e1ff;
    }

    .buttonContainer{
        display:flex;
        align-items:center;
        height:100%;
        justify-content:center;
    }

    .button{
        height:50%;
        margin-top:20px;
    	box-shadow: 0px 0px 0px 2px #d2da4b;
        background-color:#16acc9;
        border-radius:10px;
        border:3px solid #1a5355;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:19px;
        padding:12px 37px;
        text-decoration:none;
        text-shadow:0px 2px 0px #1a5355;
    }

    .buttonDisabled{
        pointer-events: none;
        height:50%;
        margin-top:20px;
    	box-shadow: 0px 0px 0px 2px #d2da4b;
        background-color:#black;
        border-radius:10px;
        border:3px solid #1a5355;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:19px;
        padding:12px 37px;
        text-decoration:none;
        text-shadow:0px 2px 0px #1a5355;
    }
 
        `;