import { LitElement, html } from 'lit-element';
import styles from './app-file-list-styles.js';



class AppFileList extends LitElement {

    static get properties() {
        return {
            name: { type: String },
            status: { type: String },
            species: { type: String },
            gender: { type: String },
            img: { type: String },
            locationUrl: { type: String },
            locationName: { type: String },
            disabledLocation: { type: Boolean}
        };
    }

    constructor() {
        super();
        this.disabledLocation = false;
    }

    static get shadyStyles() {
        return `
            ${styles.cssText}
          `;
    }

    render() {
        return html`
        <style>
            ${this.constructor.shadyStyles}
        </style>
            <div class="ficha">
                    <div class="img">
                        <img src="${this.img}">
                    </div>
                    <div class="data">
                        <h2 class="titleDiv">
                            <span class="colorTitle">Name:</span> ${this.name}
                        </h2>
                        <span class="dataDiv">
                            <span class="colorTitle">Status:</span> ${this.status}
                        </span>
                        <span class="dataDiv">
                            <span class="colorTitle">Especie:</span> ${this.species}
                        </span>
                        <span class="dataDiv">
                            <span class="colorTitle">Género:</span> ${this.gender}
                        </span>
                        <div class="buttonContainer">
                            <button 
                                class="${this.disabledLocation ? "buttonDisabled" : "button"}"
                                @click="${this.getLocation}"
                                ?disabled="${this.disabledLocation}"
                            >
                            <strong>${this.locationName}</strong>
                            </button>
                        </div>
                    </div>
            </div>
        `;
    }

    getLocation(e) {
        this.dispatchEvent(
            new CustomEvent("get-location", {
                detail: {
                    url: this.locationUrl
                }
            })
        );
    }

}

customElements.define('app-file-list', AppFileList);
