import { LitElement, html } from 'lit-element';
import '../app-dm/app-dm.js';
import styles from './app-sidebar-styles.js';



class AppSidebar extends LitElement {

    static get properties() {
        return {
            pagePrevious: { type: Boolean },
            idPrevious: { type: String },
            pageNext: { type: Boolean },
            idNext: { type: String },
            pageCharacters: { type: Boolean },
            page: { type: String },
            maxPages: { type: String },
            title: { type: String },
            subTitle: { type: String }
        };
    }

    static get shadyStyles() {
        return `
            ${styles.cssText}
          `;
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <app-dm></app-dm>
            <style>
                ${this.constructor.shadyStyles}
            </style>
            <div class="aside">
                <aside>
                    <section>
                        <div class="wrapper-buttons">
                            <button 
                            class="${this.pagePrevious ? "paginacionDisabled" : "button-paginacion" }"  
                            @click="${this.callPrevPage}" 
                            ?disabled="${this.pagePrevious}">
                            <strong>Previous</strong> </button>
                            <div class="page">
                                <div class="titlePage">${this.title}</div>
                                <span class="counter">Page ${this.page} of ${this.maxPages}</span>
                            </div>
                            <button 
                                class="${this.pageNext ? "paginacionDisabled" : "button-paginacion" }" 
                                @click="${this.callNextPage}"
                                ?disabled="${this.pageNext}">
                                <strong>Next</strong>
                            </button>
                        </div>
                    </section>
                    <section>
                        <div class="wrapper-location" id="idTitleCharacters">
                            <button 
                                id="idButtonCharacters"
                                class="${this.pageCharacters ? "buttonCharactersDisable" : "buttonCharacters"}"
                                @click="${this.callIndexPage}"
                                ?disabled="${this.pageCharacters}"><strong>Characters</strong></button>
                        </div>
                        <div class="subTitle">${this.subTitle}</div>
                    </section>
                </aside>
            </div>
        `;
    }

    callPrevPage() {
        this.shadowRoot.querySelector("app-dm").url = this.idPrevious;
    }

    callNextPage() {
        this.shadowRoot.querySelector("app-dm").url = this.idNext;
    }

    callIndexPage() {
        this.shadowRoot.querySelector("app-dm").url = "";
        this.shadowRoot.querySelector("app-dm").url = "https://rickandmortyapi.com/api/character/";
    }

}

customElements.define('app-sidebar', AppSidebar);
