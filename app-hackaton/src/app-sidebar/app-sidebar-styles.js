import { css } from 'lit-element';

export default css`

    .aside{
        position:sticky;
    }
     .wrapper-buttons{
        width:100%;
        background-color:#2dcdcc;
         display: flex;
         justify-content:center;
         flex-direction:row;
         justify-content: space-around;
     }
     .wrapper-location{
         background-color:#24282f;
         display:flex;
         justify-content:center;
     }

     .page{
         display:flex;
         align-items:center;
         font-family:sans-serif;
         font-weight:900;
         color:white;
         flex-direction:column;
         justify-content:center;
     }
     .titlePage{
        font-size:1.5rem;
     }
     .counter{
        font-size:.75rem;
     }

     .subTitle{
         display:flex;
         padding:20px;
        justify-content:center;
        color:white;
        font-size:1rem;
        font-family:sans-serif;
     }

     .button-paginacion{
        margin-bottom:20px;
        height:50%;
        margin-top:20px;
    	box-shadow: 0px 0px 0px 2px #d2da4b;
        background-color:#16acc9;
        border-radius:10px;
        border:3px solid #1a5355;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:19px;
        padding:12px 37px;
        text-decoration:none;
        text-shadow:0px 2px 0px #1a5355;
     }

     .paginacionDisabled{
        pointer-events: none;
        margin-bottom:20px;
        height:50%;
        margin-top:20px;
    	box-shadow: 0px 0px 0px 2px #d2da4b;
        background-color:grey;
        border-radius:10px;
        border:3px solid #1a5355;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:19px;
        padding:12px 37px;
        text-decoration:none;
        text-shadow:0px 2px 0px #1a5355;
     }
     
     .buttonCharacters{
        margin-bottom:20px;
        height:50%;
        margin-top:20px;
    	box-shadow: 0px 0px 0px 2px #d2da4b;
        background-color:#16acc9;
        border-radius:10px;
        border:3px solid #1a5355;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:19px;
        padding:12px 37px;
        text-decoration:none;
        text-shadow:0px 2px 0px #1a5355;
     }

     .buttonCharactersDisable{
        pointer-events: none;
        margin-bottom:20px;
        height:50%;
        margin-top:20px;
    	box-shadow: 0px 0px 0px 2px #d2da4b;
        background-color:#black;
        border-radius:10px;
        border:3px solid #1a5355;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:19px;
        padding:12px 37px;
        text-decoration:none;
        text-shadow:0px 2px 0px #1a5355;
     }
  
        `;