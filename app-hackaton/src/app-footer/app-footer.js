import { LitElement, html } from 'lit-element';
import styles from './app-footer-styles';



class AppFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    static get shadyStyles() {
        return `
          ${styles.cssText}
        `;
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <style>${this.constructor.shadyStyles}</style>
            <div class="footer">
                <div class="footer-text-wrapper">
                    <p> All rights reserved ©</p>
                    <img src="./../../assets/Logo-BBVA.jpeg" />
                    <p>Done by Ricardo, Marcos & Jose</p>
                </div>
            </div>
        `;
    }

}

customElements.define('app-footer', AppFooter);
