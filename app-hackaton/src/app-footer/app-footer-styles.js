import { css } from "lit-element";

export default css`
  :host {
    
  }

  .footer{
      background-color: black;
      color: white;
      bottom: 0;
      width: 100%;
      height: 80px;
  }

  .footer-text-wrapper{
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    margin-top: 20px;
  }
  
  .footer-text-wrapper p{
    margin-top: 35px;
  }

  img{
    width: 100px;
    margin-top: 20px;
  }
 
  
`;